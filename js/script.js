window.onload = function () {


    function startRecognizer(){
        if ('webkitSpeechRecognition' in window) {
            let recognition = new webkitSpeechRecognition();
            recognition.lang = 'ru';

            recognition.onresult = function (event) {

                let result = event.results[event.resultIndex];

                let  voice = window.speechSynthesis;
                switch (result[0].transcript) {
                    case "можешь изменить цвет":
                        let r=Math.random()*200,
                            g=Math.random()*200,
                            b=Math.random()*200;
                        document.body.style.backgroundColor=`rgb(${r}, ${g}, ${b})`;
                        thisvoice = new SpeechSynthesisUtterance("да я изменил");
                        thisvoice.lang = "ru-RU";
                        voice.speak(thisvoice);

                        break;
                    case "Как тебя зовут":
                        thisvoice = new SpeechSynthesisUtterance("меня зовут ходор");
                        thisvoice.lang = "ru-RU";
                        voice.speak(thisvoice);

                        break;
                    case "Удали все данные":
                        localStorage.clear();
                        thisvoice = new SpeechSynthesisUtterance("я удалил все данные из сториджа");
                        thisvoice.lang = "ru-RU";
                        voice.speak(thisvoice);
                        break;
                    default:
                        thisvoice = new SpeechSynthesisUtterance("я не понимаю что ты говоришь");
                        thisvoice.lang = "ru-RU";
                        voice.speak(thisvoice);
                }


            };



            recognition.start();
        } else alert('webkitSpeechRecognition не поддерживается')
    }
    document.getElementById('voice').addEventListener('click',function () {
        startRecognizer();
    });




    let data = [
        {
            id: 2,
            firstname: 'Narek',
            lastname: 'Harutyunyan',
            number: 4564646,
            age: 22,
            show: true,


        },
        {
            id: '1',
            firstname: 'Andranik',
            lastname: 'Grigoryan',
            number: 252545,
            age: 47,
            show: true,

        },
        {
            id: 3,
            firstname: 'Babken',
            lastname: 'Suqiasya',
            number: 25254,
            age: 45,
            show: true,
        },
        {
            id: 4,
            firstname: 'Harut',
            lastname: 'Ghazaryan',
            number: 25254,
            age: 45,
            show: true,
        },
        {
            id: 6,
            firstname: 'Shushan',
            lastname: 'Poskalyan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 5,
            firstname: 'varazdat',
            lastname: 'Mamikonyan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 7,
            firstname: 'Frunz',
            lastname: 'Hambardzumyan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 9,
            firstname: 'Armen',
            lastname: 'Tumanyan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 8,
            firstname: 'Babken',
            lastname: 'Grigoryan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 10,
            firstname: 'Mane',
            lastname: 'Grigoryan',
            number: 25254,
            age: 45,
            show: true,
        },

        {
            id: 11,
            firstname: 'Davo',
            lastname: 'Grigoryan',
            number: 25254,
            age: 45,
            show: true,
        },
        {
            id: 12,
            firstname: 'Suro',
            lastname: 'Grigoryan',
            number: 25254,
            age: 45,
            show: true,
        },


    ];


    function DataTable(obj, dataTadleId) {

        if (window.localStorage.getItem(dataTadleId)) {
            let DataObj = JSON.parse(window.localStorage.getItem(dataTadleId));
            obj.data = DataObj;


        } else {
            window.localStorage.setItem(dataTadleId, JSON.stringify(obj.data));
            let DataObj = JSON.parse(window.localStorage.getItem(dataTadleId));
            obj.data = DataObj;



        }




        let div=document.createElement('div');
            div.setAttribute('id',dataTadleId);
            blocks.appendChild(div);
        if (obj.data.length <= 0) {
            let div=document.createElement('div');
            div.innerHTML="<h1 align='center'>Data  is Empty</h1>";
            div.innerHTML+="<hr style='border: 3px dashed black;'>"
            div.style.height="250px";
            div.style.weight="250px";

            document.querySelector ('#' + dataTadleId ).innerHTML="";
            return  document.querySelector ('#' + dataTadleId ).appendChild(div);

        } else if ( obj.hasOwnProperty('colums')===false ) {


            /*let div=document.createElement('div');
            div.innerHTML="<h1 align='center'>Data Table Columns is empty</h1>";
            div.innerHTML+="<hr style='border: 3px dashed black;'>"
            div.style.height="250px";
            div.style.weight="250px";

            document.querySelector ('#' + dataTadleId ).innerHTML="";
            return  document.querySelector ('#' + dataTadleId ).appendChild(div);*/
            let columns=Object.keys(obj.data[0]);
            let cols=[];
            for (let i = 0; i <columns.length ; i++) {
                if(columns[i]!=="show" && columns[i]!=="page"){
                    cols.push(columns[i]);
                }
            }

            obj.colums=cols;

        }else if((obj.colums.length <= 0) ){
            let columns=Object.keys(obj.data[0]);
            for (let i = 0; i <columns.length ; i++) {
                if(columns[i]==="show" || columns[i]==='page'){
                    let index = columns.indexOf(columns[i]);
                    if (index > -1) {
                        columns.splice(index, 1);
                    }
                }
            }
            obj.colums=columns;
        }
        let block = document.getElementById(dataTadleId),
            table = document.createElement('table'),
            thead = document.createElement('thead'),
            tbody = document.createElement('tbody'),
            objtwo = [],
            l,
            next = 1,

            col = obj.colums;


        table.setAttribute('class', 'table table-hover table-dark');
        table.style.marginTop = '20px';


        const additionalView = setAdditionalView();
        block.innerHTML = additionalView;
        let row=document.createElement('div'),
            colsm=document.createElement('div');
        row.setAttribute('class','row');
        colsm.setAttribute('class','offset-sm-2 col-sm-8');
        colsm.style.overflow='scroll';
        colsm.style.width=obj.width;
        colsm.style.height=obj.height;


        colsm.appendChild(table)
        row.appendChild(colsm);
        document.querySelector ('#' + dataTadleId + ' #after').after(row)
        /*block.prepend(table);*/
        let searchRes = document.querySelector ('#' + dataTadleId + ' .search');
        let pagination = document.querySelector ('#' + dataTadleId + ' .pagination');
        let rendertd;
        //
        // if (obj.hasOwnProperty('renderActions')){
        //     console.log(obj.renderActions);
        // }


        function showtrue() {
            for (let i = 0; i < obj.data.length; i++) {
                obj.data[i].show = true;
            }
        }

        let css = 'table > thead>tr> td:hover{ background-color: #00ff00;cursor:pointer };',
            style = document.createElement('style');

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        document.getElementsByTagName('head')[0].appendChild(style);

        let temp = 0;


        for (let i = 0; i < obj.data.length; i++) {
            let p = {};
            for (let j = 0; j < col.length; j++) {

                if (obj.data[i].hasOwnProperty(col[j])) {
                    p[col[j]] = obj.data[i][col[j]];
                }

            }
            objtwo.push(p)
        }
        obj.data = objtwo;

        let select = `
                    <select class="form-control"  id="sel">
                            <option value="5" class="op">5</option>
                            <option value="10" class="op">10</option>
                            <option value="15" class="op">15</option>
                     </select>
        `;

        document.querySelector ('#' + dataTadleId + ' .select').innerHTML = select;
        l = document.getElementsByClassName('op')[0].value;

//todo

        document.querySelector ('#' + dataTadleId + ' .select').addEventListener('change', function (e) {


            l = e.target.value;
            next = 1;


            showtrue();
            paginate(obj.data);
            tHead(col);
            Tbody(obj.data);


        });

        function paginate(ob) {

            let g = 1,
                b = 1,
                iteration = +l;
            document.querySelector ('#' + dataTadleId + ' .pagination').innerHTML = '';
            let mod = ob.length % iteration;
            for (let i = 1; i <= Math.round(ob.length / iteration); i++) {
                document.querySelector ('#' + dataTadleId + ' .pagination').innerHTML += `<li style="cursor: pointer" class="page-item"  ><a class="page-link page"  >${i}</a></li>`
            }
            if (ob.length <= 2) {
                document.querySelector ('#' + dataTadleId + ' .pagination').innerHTML += `<li style="cursor: pointer" class="page-item"  ><a class="page-link page"   >1</a></li>`
            } else if (mod <= iteration) {
                let lastpage =  document.querySelectorAll ('#' + dataTadleId + ' .page');



                document.querySelector ('#' + dataTadleId + ' .pagination').innerHTML += `<li class="page-item" style="cursor: pointer"  ><a class="page-link page"    >${+lastpage[lastpage.length - 1].innerText + 1}</a></li>`;
            }
            for (let i = 0; i < ob.length; i++) {

                if (g <= l) {
                    ob[i].page = b;
                    ++g;
                } else {
                    ++b;
                    ob[i].page = b;
                    g = 2;
                }
            }
            let page = document.querySelectorAll ('#' + dataTadleId + ' .page');


            for (let i = 0; i < page.length; i++) {
                page[i].addEventListener('click', function () {
                    next = this.innerText;


                    Tbody(obj.data)

                })
            }
            return ob;
        }


        obj.data = obj.data.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));
        paginate(obj.data);
        let sort = document.getElementsByClassName('sorthby'+dataTadleId);


        function tHead(col) {
            if (document.querySelector ('#' + dataTadleId + ' thead')) {
                document.querySelector ('#' + dataTadleId + ' thead').innerHTML = '';
            }
            let tr = document.createElement('tr');

            for (let i = 0; i < col.length; i++) {
                let td = document.createElement('td');
                /*td.innerText=col[i];*/
                td.innerHTML = `<i class="fa fa-arrow-down" style="color: blue" aria-hidden="true">${col[i]} </i> `;
                td.setAttribute('id', col[i]);
                td.setAttribute('class', 'sorthby'+dataTadleId);
                tr.appendChild(td)
            }
            thead.appendChild(tr);
            table.appendChild(thead);
            Sorth()

        }

        function Tbody(data) {
            if (document.querySelector ('#' + dataTadleId + ' tbody')) {
                document.querySelector ('#' + dataTadleId + ' tbody').innerHTML = '';
            }

            let arrdata = [];
            let arrcol = [...col];

            for (let i = 0; i < data.length; i++) {

                if (data[i].show === true) {
                    let tr = document.createElement('tr');
                    tr.setAttribute('class', 'edit');
                    tr.style.cursor = 'pointer';
                    tr.setAttribute('id', ` ${data[i].id}`);
                    tr.addEventListener('click', function (e) {

                        e.type = "";

                        let array = this.children,
                            id = this.id;
                        for (let j = 0; j < array.length; j++) {
                            let name = array[j].innerText;


                            array[j].innerHTML = `<input type="text" value="${name}" class="editinput" >`;
                            if ((array.length - 1) === j) {
                                array[j].innerHTML += `<input type="submit" class="btn btn-success ${id}" id="editbtn" style="margin-left:10px" value="Edit" >`;
                                array[j].innerHTML += `<input type="submit" class="btn btn-danger ${id}" id="delbtn" style="margin-left:10px" value="Delete" >`;
                            }
                        }
                        let cl = document.getElementsByClassName(`${id}`)[0],
                            del = document.getElementsByClassName(`${id}`)[1];
                        del.addEventListener('click', function (e) {
                            e.preventDefault();
                            let r = this.classList[2];
                            for (let j = 0; j < obj.data.length; j++) {

                                if (obj.data[j].id == r) {

                                    var index = obj.data.indexOf(obj.data[j]);

                                    if (index > -1) {
                                        obj.data.splice(index, 1);
                                    }


                                }
                            }
                            localStorage.removeItem(dataTadleId);
                            window.localStorage.setItem(dataTadleId, JSON.stringify(obj.data));
                            let DataObj = JSON.parse(window.localStorage.getItem(dataTadleId));
                            obj.data = DataObj;

                            paginate(obj.data);
                            Tbody(obj.data)


                        });
                        cl.addEventListener('click', function (e) {

                            e.preventDefault();
                            let r = this.classList[2];
                            let p = document.getElementsByClassName('editinput');

                            let arrinputs = {};
                            for (let j = 0; j < p.length; j++) {

                                arrinputs[col[j]] = p[j].value;
                            }
                            for (let j = 0; j < obj.data.length; j++) {
                                if (obj.data[j].id == r) {
                                    for (let prop in arrinputs) {
                                        let type = typeof obj.data[j][prop];

                                        if (type === 'string') {
                                            obj.data[j][prop] = String(arrinputs[prop]);


                                            paginate(obj.data)

                                        } else if (type === 'number') {
                                            obj.data[j][prop] = Number(arrinputs[prop]);
                                            obj.data = obj.data.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));
                                            paginate(obj.data)
                                        }


                                    }
                                    localStorage.removeItem(dataTadleId);
                                    window.localStorage.setItem(dataTadleId, JSON.stringify(obj.data));
                                    let DataObj = JSON.parse(window.localStorage.getItem(dataTadleId));
                                    obj.data = DataObj;


                                    Tbody(obj.data)
                                }
                            }


                        })
                    }, {once: true});
                    if (data[i].page == next) {

                        for (let prop in data[i]) {
                            if (col.includes(prop)) {
                                let td = document.createElement('td');
                                    arrdata.push(prop);
                                    td.innerText = data[i][prop];
                                    tr.appendChild(td);
                                if (obj.hasOwnProperty('renderActions') && obj.renderActions[prop]){
                                    td.innerHTML = obj.renderActions[prop].render(data[i]);
                                    tr.appendChild(td);
                                    console.log(obj.renderActions[prop].render(data[i]));
                                }
                            }

                        }
                    }


                    arrdata = arrdata.filter(function (elem, index, self) {
                        return index === self.indexOf(elem);
                    });

                    tbody.appendChild(tr)
                }
            }


            for (let i = 0; i < arrdata.length; i++) {
                for (let j = 0; j < arrcol.length; j++) {
                    if (arrdata[i] === arrcol[j]) {

                        var index = arrcol.indexOf(arrcol[j]);
                        if (index > -1) {
                            arrcol.splice(index, 1);
                        }
                    }
                }
            }

            for (let i = 0; i < arrcol.length; i++) {
                let head = document.getElementById(arrcol[i])
                if (head) {
                    head.remove()
                }
            }


            table.appendChild(tbody)
        }


        showtrue();
        tHead(col);
        Tbody(obj.data);
        let searching;
        if(obj.hasOwnProperty('searchTo')===true){
            searching=obj.searchTo;
        }else{
            searching="All"
        }
        searchRes.innerHTML = `<input type="text" style="margin-top:10px" class="form-control" placeholder="Search by ${searching}" id="search">`;
        let search = document.querySelector ('#' + dataTadleId + ' .search');


        search.addEventListener('input', function (e) {

            let element=Array.from(sort);
            let search = e.target.value;
            let searchto = obj.searchTo;

            if (search == '') {
                for (let i = 0; i < obj.data.length; i++) {
                    obj.data[i].show = true
                }

                tHead(col);
                Tbody(obj.data)
            }
            if(obj.hasOwnProperty('searchTo')===false){
                next:for (let i = 0; i < obj.data.length; i++) {

                    for (let j = 0; j <element.length ; j++) {

                        let tru = String(obj.data[i][element[j].id]).toLowerCase().includes(search.toLowerCase());
                        if (tru == false) {
                            obj.data[i].show = false;
                        } else {
                            obj.data[i].show = true;
                            continue next;
                        }
                    }

                }
                let  arrsearch=[...obj.data];
                let newar=[];
                arrsearch.map((el)=>{
                    if(el.show===true ){
                        newar.push(el)

                    }
                });


                paginate(newar);
                tHead(col);
                Tbody(newar);

            }else{
                for (let i = 0; i < obj.data.length; i++) {

                    let tru = String(obj.data[i][searchto]).toLowerCase().includes(search.toLowerCase());
                    if (tru == false) {
                        obj.data[i].show = false;
                    } else {
                        obj.data[i].show = true;
                    }
                }

                tHead(col);
                Tbody(obj.data);

            }


        });


        function Sorth() {

            for (let i = 0; i < sort.length; i++) {


                sort[i].addEventListener('click',sorting )
                function sorting() {

                    let sorthby = this.id;
                    let type = typeof obj.data[0][sorthby];

                    if (type === 'string') {
                        if (temp % 2 === 0) {
                            obj.data = obj.data.sort(function (a, b) {
                                if (a[sorthby] < b[sorthby]) {
                                    return -1;
                                }
                                if (a[sorthby] > b[sorthby]) {
                                    return 1;
                                }
                                return 0;
                            });

                            this.innerHTML = `<i class="fa fa-arrow-up" style="color: blue" aria-hidden="true">${sorthby} </i> `;
                            temp++;
                            Tbody(obj.data)
                        } else {
                            obj.data = obj.data.sort(function (a, b) {
                                if (a[sorthby] > b[sorthby]) {
                                    return -1;
                                }
                                if (a[sorthby] < b[sorthby]) {
                                    return 1;
                                }
                                return 0;
                            });
                            this.innerHTML = `<i class="fa fa-arrow-down" style="color: blue" aria-hidden="true">${sorthby} </i> `;
                            temp++;
                            Tbody(obj.data)
                        }


                    } else if (type === 'number') {
                        if (temp % 2 === 0) {
                            obj.data = obj.data.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
                            this.innerHTML = `<i class="fa fa-arrow-up" style="color: blue" aria-hidden="true">${sorthby} </i> `;
                            temp++
                            Tbody(obj.data)
                        } else {
                            obj.data = obj.data.sort((a, b) => parseFloat(a.id) - parseFloat(b.id));
                            this.innerHTML = `<i class="fa fa-arrow-down" style="color: blue" aria-hidden="true">${sorthby} </i> `;
                            temp++
                            Tbody(obj.data)
                        }

                    }
                }
                if(obj.hasOwnProperty('columnSorthDisable')===true){
                    if(sort[i].id==obj.columnSorthDisable){
                        sort[i].removeEventListener('click',sorting)
                    }
                }
            }

        }

        let up = document.querySelector ('#' + dataTadleId + ' #up')
        up.innerHTML += `<button type="button" id='pdf' style="margin-left: 10px;margin-top: 10px;" class="btn btn-success">PDF</button>`;
        up.innerHTML += `<button type="button" id='csv' style="margin-left: 10px;margin-top: 10px" class="btn btn-success">CSV</button>`;
        up.innerHTML += `<button type="button" id='print' style="margin-left: 10px;margin-top: 10px" class="btn btn-success">Print</button>`;
        let addinputs = Array.from(sort);
        addinputs.map((el) => {

            if (el.id !== 'id') {
                document.querySelector ('#' + dataTadleId + ' #inputs').innerHTML += ` <div class="col">
                                    <input type="text" class="form-control info" id=${el.id} style="margin-top: 10px" placeholder=${el.id}>
                                </div>`
            }

        });
        document.querySelector ('#' + dataTadleId + ' .adddata').addEventListener('click', function (e) {
            e.preventDefault();

            let info =  document.querySelectorAll ('#' + dataTadleId + ' .info');

            let data = {};

            let lastid = obj.data[obj.data.length - 1]['id'];


            for (let i = 0; i < info.length; i++) {
                if (info[i].value !== "") {


                    data[info[i].id] = info[i].value
                    info[i].value = "";
                } else {
                    return alert(`Input ${info[i].id} is Empty`)
                }
                if (i === info.length - 1) {
                    data['id'] = lastid + 1

                }
            }


            obj.data.push(data);


            let example = [];
            for (let i = 0; i < obj.data.length; i++) {
                let p = {};
                for (let j = 0; j < col.length; j++) {

                    if (obj.data[i].hasOwnProperty(col[j])) {
                        p[col[j]] = obj.data[i][col[j]];
                    }

                }
                example.push(p)
            }

            obj.data = example;
            showtrue();
            paginate(obj.data);


            localStorage.removeItem(dataTadleId);
            window.localStorage.setItem(dataTadleId, JSON.stringify(obj.data));
            let DataObj = JSON.parse(window.localStorage.getItem(dataTadleId));

            tHead(col);
            Tbody(DataObj)


        });
        document.querySelector ('#' + dataTadleId + ' #pdf').addEventListener('click', function () {
            let doc = new jsPDF();


            let arrhead = [];
            let arrbody = [];

            for (let i = 0; i < sort.length; i++) {
                arrhead.push(sort[i].innerText)
            }

            for (let i = 0; i < obj.data.length; i++) {
                let arr = [];
                for (let j = 0; j < arrhead.length; j++) {
                    arr.push(obj.data[i][arrhead[j]])


                }
                arrbody.push(arr)
            }


            doc.autoTable({
                head: [arrhead],
                body: arrbody
            });

            doc.save('Table.pdf');

        });


        let arrhead = [];
        let arrbody = [];
        let data = [];

        function Tabletrtd() {

            arrhead = [];
            arrbody = [];

            for (let i = 0; i < sort.length; i++) {
                arrhead.push(sort[i].innerText)
            }

            for (let i = 0; i < obj.data.length; i++) {
                let arr = [];
                for (let j = 0; j < arrhead.length; j++) {
                    arr.push(obj.data[i][arrhead[j]])


                }
                arrbody.push(arr)
            }
        }

        document.querySelector ('#' + dataTadleId + ' #csv').addEventListener('click', function () {


            data = [];
            Tabletrtd();
            data.push(arrhead);


            for (let i = 0; i < arrbody.length; i++) {
                data.push(arrbody[i])
            }

            let csv = '\n';
            data.forEach(function (row) {
                csv += row.join(',');
                csv += "\n";
            });


            let hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
            hiddenElement.target = '_blank';
            hiddenElement.download = 'Datatable.csv';
            hiddenElement.click();

        });


        document.querySelector ('#' + dataTadleId + ' #print').addEventListener('click', function () {

            data = [];
            Tabletrtd();
            data.push(arrhead);


            for (let i = 0; i < arrbody.length; i++) {
                data.push(arrbody[i])
            }
            let table = document.createElement('table')
            for (let i = 0; i < data.length; i++) {
                let tr = document.createElement('tr');
                if (i === 0) {
                    tr.setAttribute('class', 'color')
                }
                for (let j = 0; j < data[i].length; j++) {
                    let td = document.createElement('td');


                    td.innerText = data[i][j];
                    tr.appendChild(td)
                }
                table.appendChild(tr)
            }

            let print = table.outerHTML;


            newWin = window.open("");
            let style = `<style media="print">
                    table{
                        background-color: dimgray;
                        width: 100%;
                        font-size: 20px;
                        
                    }
                    table,tr,td{
                        border: 4px solid black;
                    }
                    .color{
                        background-color: darkturquoise;
                        font-weight: bold;
                    }
                    </style>`;
            newWin.document.write(style);
            newWin.document.write(print);
            newWin.print();
            newWin.close();

        })


    }

    function setAdditionalView() {
        return '<div class="container-fluid">'+'<div class="row"> <div class="col-sm-2"></div>' +
            '<div class="col-sm-2 search"> </div>' +
            '<div class="col-sm-3"></div>' +
            '<div class="col-sm-3 select">' +
            '</div>'+
            '</div>'+ '<div class="row">'+
        '<div class="col-sm-2"></div>'+
        '<div class="col-sm-3" style="margin-top: 10px" id="up">'+


        '</div>'+
        '<div class="col-sm-7" ></div>'+
        '</div>'+'<div class="row" id="after">'+
            '<div class="col-sm-2"></div>'+
            '<div class="col-sm-7" >'+
            '<div class="row " id="inputs">'+



            '</div>'+

            '</div>'+
            '<div class="col-sm-2"><button type="button"  style="margin-top: 10px" class="btn btn-info adddata">Add</button></div>'+
        '</div>'+'<div class="row">'+
        '<div class="col-sm-2"></div>'+
        ' <div class="col-sm-4">'+
        '<ul  class="pagination">'+


        '</ul>'+
        '</div>'+
        '</div>'+
            '</div>'+'<hr style="border: 3px dashed black;">';


    }

    DataTable({
        data: data,

        renderActions: {
             firstname: {
                 column: 'firstname',
                 render: function (row) {
                     return row['firstname'] + '<input type="checkbox" style="background-color: blue" >';
                 }
             },
             age: {
                 column: 'lastname',
                 render: function (row) {
                     return '<h5>' + row['lastname'] + '</h5>';
                 }
             }
         },

        columnSorthDisable:'firstname',
        width:'100px',
        height:"350px",

    }, 'block');

    DataTable({
        data: [],
        colums: ['id', 'color'],
        searchTo: 'color',
        width:'700px',
        height:"200px",
    }, 'block2');
    DataTable({
        data: [ {
            id: 1,
            firstname: 'Zaqar',
            color:'yellow',
            height2:"45px",
            height3:"45px",
            height4:"45px",
            height5:"45px",
            height:"45px",

            number: 252545,
            age: 47,
            show: true,

        },
            {
                id: 2,
                firstname: 'Arsen',
                number: 252545,
                height2:"45px",
                height3:"45px",
                height4:"45px",
                height5:"45px",
                height:"45px",
                age: 23,
                color:'red',
                height:"45px",
                show: true,

            },
        ],

        searchTo: 'id',
        width:"50px",
        height:"200px",

    }, 'block3');






};